<?php

use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\FormRequestHandler;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\Storage\AssetStore;
use SilverStripe\Security\Permission;
use SilverStripe\Versioned\Versioned;
use SilverStripe\Control\Controller;
use SilverStripe\ORM\DataObject;
use gorriecoe\Link\Models\Link;
use gorriecoe\LinkField\LinkField;

class Slide extends DataObject
{
    private static $db = [
        "SortSlide" => "Int",
        'Title' => 'Varchar(255)'
    ];

    private static $has_one = [
        'Page' => HomePage::class,
        'Afbeelding' => Image::class
    ];

    private static $owns = [
        'Afbeelding'
    ];

    public function getThumbnail()
    {
        if ($this->Afbeelding()->exists()) {
            return $this->Afbeelding()->Pad(200, 100);
        } else {
            return 'Geen afbeelding';
        }
    }

    private static $summary_fields = [
        'Title' => 'Titel',
        'Thumbnail' => 'Afbeelding'
    ];

    private static $extensions = [
        Versioned::class
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldToTab('Root.Main', TextField::create("Title", "Titel"));

        $fields->addFieldToTab("Root.Main", $Afbeelding = UploadField::create('Afbeelding', 'Afbeelding'));
        $Afbeelding->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
        $Afbeelding->setAllowedMaxFileNumber(1);

        $fields->removeFieldFromTab("Root.Main", "PageID");
        $fields->removeFieldFromTab("Root.Main", "SortSlide");

        return $fields;
    }


    protected function onAfterDelete()
    {
        if ($this->AfbeeldingID) {
            $Afbeelding = $this->Afbeelding();
            $Afbeelding->delete();
            $Afbeelding->destroy();
        }

        parent::onAfterDelete();
    }

    protected function onBeforeWrite()
    {
        if (!$this->Title) {
            $this->Title = $this->Afbeelding()->Title;
        }
        if (!$this->Sort) {
            $this->Sort = Banner::get()->max('Sort') + 1;
        }
        parent::onBeforeWrite();
    }
}
