<?php

use SilverStripe\Forms\TextField;
use SilverStripe\Security\Permission;
use SilverStripe\Versioned\Versioned;
use SilverStripe\Control\Controller;
use SilverStripe\ORM\DataObject;

class Gebied extends DataObject
{
    private static $db = [
        'Title' => 'Varchar(255)'
    ];

    private static $has_one = [
        'ContactPage' => ContactPage::class
    ];

    private static $summary_fields = [
        'Title' => 'Gebied'
    ];

    private static $extensions = [
        Versioned::class
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldToTab('Root.Main', TextField::create("Title", "Gebied"));

        $fields->removeFieldFromTab("Root.Main", "ContactPageID");

        return $fields;
    }
}
