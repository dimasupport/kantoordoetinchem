<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\LiteralField;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\NumericField;
    use SilverStripe\Forms\EmailField;
    use SilverStripe\Forms\TextareaField;
    use SilverStripe\Forms\CheckboxField;
    use SilverStripe\Forms\FormAction;
    use SilverStripe\Forms\Form;
    use SilverStripe\Forms\DropdownField;
    use SilverStripe\Forms\HiddenField;
    use SilverStripe\Control\Email\Email;
    use SilverStripe\Forms\RequiredFields;
    use SilverStripe\Control\HTTPRequest;
    use SilverStripe\SiteConfig\SiteConfig;
    use SilverStripe\Control\Controller;
    use SilverStripe\View\Requirements;

    class ContactPageController extends PageController
    {
        private static $allowed_actions = [
        'ContactFormulier'
      ];

        public function ContactFormulier()
        {
            Requirements::javascript('https://www.google.com/recaptcha/api.js?render=6Lfwh6sUAAAAAHOVfhTdjY_JQkFfWpLDnKg3GlCu');

            //Form Fields
            $config = SiteConfig::current_site_config();
            $PrivacyLink = $config->Privacyverklaring();
            $DivPrivacy = LiteralField::create('DivPrivacy', '<div class="privacy">');
            $PrivacyTekst = LiteralField::create('PrivacyTekst', '<p>'._t("ContactForm.PrivacyAkkoord", "Ik ga akkoord met de").' <a href="'.$PrivacyLink->LinkURL.'" target="_blank">'._t("ContactForm.Privacyverklaring", "privacyverklaring").'</a></p>');
            $AantalTitle = LiteralField::create('AantalTitle', '<label class="left">Aantal M²</label>');

            $PrivacyCheckbox = CheckboxField::create('PrivacyCheckbox', $PrivacyTekst);
            $PrivacyCheckbox->addExtraClass('float-left privacy-checkbox');

            $DivRow = LiteralField::create('DivRow', '<div class="row">');
            $DivCol6 = LiteralField::create('DivCol6', '<div class="col-12 col-md-12 col-lg-6">');
            $DivCol12 = LiteralField::create('DivCol12', '<div class="col-12 col-md-12 col-lg-12">');
            $DivSluiten = LiteralField::create('DivSluiten', '</div>');

            $Naam = TextField::create('Naam', 'Naam');
            $Email = EmailField::create('Email', 'E-mail');
            $Telefoon = NumericField::create('Telefoonnummer', 'Telefoonnummer');
            $Van = TextField::create('Van');
            $Van->setTitle(0);
            $Tot = TextField::create('Tot');
            $Tot->setTitle(0);
            $Pandsoorten = DropdownField::create('Pandsoort', 'Soort bedrijfspand', $this->Pandsoorten()->map('Title', 'Title')->toArray());
            $Pandsoorten->setEmptyString('Selecteer een optie');
            $Gebieden = DropdownField::create('Gebied', 'Voorkeursgebied', $this->Gebieden()->map('Title', 'Title')->toArray());
            $Gebieden->setEmptyString('Selecteer een optie');
            $Opmerking = TextareaField::create('Bericht', 'Opmerking');

            $GoogleCaptcha = HiddenField::create('g-recaptcha-response');

            //Form Action
            $Versturen = FormAction::create('VerstuurContactFormulier', _t("ContactForm.Versturen", "Aanvraag versturen"));
            $Versturen->setUseButtonTag(true);
            $Versturen->addExtraClass('btn btn-uitgelicht');

            $Actions = FieldList::create(
                $Versturen
        );

            //Fieldlist
            $Fields = FieldList::create(
                $DivRow,
                $DivCol12,
                $Naam,
                $Email,
                $Telefoon,
                $DivRow,
                $DivCol12,
                $AantalTitle,
                $DivSluiten,
                $DivCol6,
                $Van,
                $DivSluiten,
                $DivCol6,
                $Tot,
                $DivSluiten,
                $DivSluiten,
                $Pandsoorten,
                $Gebieden,
                $Opmerking,
                $DivSluiten,
                $DivCol12,
                $DivPrivacy,
                $PrivacyCheckbox,
                $GoogleCaptcha,
                $DivSluiten,
                $DivSluiten,
                $DivSluiten
        );

            //Required Fields
            $Validator = RequiredFields::create([
          'Naam',
          'Email',
          'Telefoonnummer',
          'Van',
          'Tot',
          'Pandsoort',
          'Gebied',
          'Bericht',
          'PrivacyCheckbox'
        ]);
    
            //Create Form
            $form = Form::create($this, __FUNCTION__, $Fields, $Actions, $Validator);

            return $form;
        }

        public function VerstuurContactFormulier($data, $form)
        {
            if (isset($data["g-recaptcha-response"]) && !empty($data["g-recaptcha-response"])) {
                $secret = '6Lfwh6sUAAAAAC0AmXZE_0qrLFJuzYLhomII2KJ7';
                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$data["g-recaptcha-response"]);
                $responseData = json_decode($verifyResponse);
                if ($responseData->success) {
                    //Send to
                    $SiteConfig = SiteConfig::current_site_config();
                    $EmailTo = $SiteConfig->FormulierenEmail;
                    if ($EmailTo) {
                        $To = $EmailTo;
                    } else {
                        $To = "remco@dima.nl";
                    }
                    $From = "no-reply@dimademo.nl";
                    $Subject = _t("ContactForm.Onderwerp", "Nieuw Contactbericht") .'-'. $SiteConfig->Title;

                    //Email
                    $Email = Email::create($From, $To, $Subject);
                    $Email->setHTMLTemplate('Email/ContactEmail');
                    $Email->addData([
                        'Naam' => $data['Naam'],
                        'Email' => $data['Email'],
                        'Telefoonnummer' => $data['Telefoonnummer'],
                        'Van' => $data['Van'],
                        'Tot' => $data['Tot'],
                        'Pandsoort' => $data['Pandsoort'],
                        'Gebied' => $data['Gebied'],
                        'Bericht' => $data['Bericht'],
                        'Datum'=> date("d-m-Y"),
                        'Tijd'=> date("H:i:s")
                    ]);
                    $Email->send();

                    $RedirectLink = Controller::join_links($this->Link(), '?contact=1');
            
                    return $this->redirect($RedirectLink);
                } else {
                    $form->sessionMessage('Uw bericht is mogelijk als foutief beschouwd, mocht dit volgens u niet het geval zijn neem dan even contact met ons op.', 'failure');
                    return $this->redirectBack();
                }
            }
        }

        public function ContactFormSuccess()
        {
            return isset($_REQUEST['contact']) && $_REQUEST['contact'] == "1";
        }


        protected function init()
        {
            parent::init();

            // You can include any CSS or JS required by your project here.
          // See: https://docs.silverstripe.org/en/developer_guides/templates/requirements/
        }
    }
}
