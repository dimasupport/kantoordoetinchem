<?php

namespace {

    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\GridField\GridField;
    use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use Symbiote\GridFieldExtensions\GridFieldOrderableRows;    
    use SilverStripe\CMS\Model\SiteTree;

    class ContactPage extends Page
    {
        private static $db = [
            'ContactFormulierTitle' => 'Varchar(255)'
        ];

        private static $has_many = [
            'Gebieden' => 'Gebied',
            'Pandsoorten' => 'Pandsoort'
        ];
        
        private static $owns = [
            'Gebieden',
            'Pandsoorten'
        ];        

        public function getCMSFields()
        {
            
            $fields = parent::getCMSFields();

            $fields->addFieldToTab("Root.Main", TextField::create("ContactFormulierTitle", "Contactformulier titel"), 'Content');

            $conf = GridFieldConfig_RecordEditor::create();
            $fields->addFieldToTab('Root.Gebieden', new GridField('Gebieden', 'Gebieden', $this->Gebieden(), $conf)); 

            $conf2 = GridFieldConfig_RecordEditor::create();
            $fields->addFieldToTab('Root.Pandsoorten', new GridField('Pandsoorten', 'Pandsoorten', $this->Pandsoorten(), $conf2));            

            $fields->removeByName("Panden");
            $fields->removeFieldFromTab("Root.Main", "ContentAfbeelding");
            $fields->removeFieldFromTab("Root.Main", "ContentButton");
              
            return $fields;
        }        
    }
}
