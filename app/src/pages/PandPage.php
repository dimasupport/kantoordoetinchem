<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\LiteralField;
    use SilverStripe\Forms\GridField\GridField;
    use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
    use gorriecoe\Link\Models\Link;
    use gorriecoe\LinkField\LinkField;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Assets\Image;
    use SilverStripe\Assets\Storage\AssetStore;

    class PandPage extends Page
    {
        private static $db = [
        ];

        private static $many_many = [
          'Afbeeldingen' => Image::class
        ];

        private static $owns = [
          'Afbeeldingen'
        ];

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();

            $fields->addFieldToTab("Root.Gallerij", $Afbeelding = TextField::create('AchtergrondTitle', 'Titel'));
            $fields->addFieldToTab("Root.Gallerij", $Afbeelding = UploadField::create('AchtergrondAfbeelding', 'Achtergrondafbeelding'));
            $fields->addFieldToTab("Root.Gallerij", $Afbeelding = UploadField::create('Afbeeldingen', 'Afbeeldingen'));
            $Afbeelding->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));

            $fields->removeByName("Panden");
              
            return $fields;
        }
    }
}
