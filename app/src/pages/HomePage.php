<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\LiteralField;
    use SilverStripe\Forms\GridField\GridField;
    use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
    use gorriecoe\Link\Models\Link;
    use gorriecoe\LinkField\LinkField;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Assets\Image;
    use SilverStripe\Assets\Storage\AssetStore;

    class HomePage extends Page
    {
        private static $db = [
          'UspTitle' => 'Varchar(255)',
          'UspContent' => 'HTMLText',

          'Usp1Icon' => 'Varchar(255)',
          'Usp1Text' => 'Varchar(255)',
          'Usp2Icon' => 'Varchar(255)',
          'Usp2Text' => 'Varchar(255)',
          'Usp3Icon' => 'Varchar(255)',
          'Usp3Text' => 'Varchar(255)',
          'Usp4Icon' => 'Varchar(255)',
          'Usp4Text' => 'Varchar(255)',
          'Usp5Icon' => 'Varchar(255)',
          'Usp5Text' => 'Varchar(255)',
          'Usp6Icon' => 'Varchar(255)',
          'Usp6Text' => 'Varchar(255)',

          'HighlightTitle' => 'Varchar(255)'
        ];

        private static $has_one = [
          'Afbeelding' => Image::class,
          'UspAfbeelding' => Image::class
        ];

        private static $many_many = [
          'Banners' => 'Banner',
          'Slides' => 'Slide',
          'Highlights' => 'Highlight'
        ];

        private static $owns = [
          'Banners',
          'Slides',
          'Highlights',
          'Afbeelding',
          'UspAfbeelding'
        ];

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();

            $conf = GridFieldConfig_RecordEditor::create();
            $conf->addComponent(new GridFieldOrderableRows('Sort'));

            $fields->addFieldToTab('Root.Banner', new GridField('Banners', 'Banners', $this->Banners(), $conf));
         
            $fields->addFieldToTab("Root.Usp's", TextField::create("UspTitle", "Usp titel"));
            $fields->addFieldToTab("Root.Usp's", HTMLEditorField::create("UspContent", "Usp content"));

            $fields->addFieldToTab("Root.Usp's", $UspAfbeelding = UploadField::create('UspAfbeelding', 'Achtergrond'));
            $UspAfbeelding->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
            $UspAfbeelding->setAllowedMaxFileNumber(1);

            $fields->addFieldToTab("Root.Usp's", TextField::create("Usp1Icon", "Usp 1 icoon"));
            $fields->addFieldToTab("Root.Usp's", TextField::create("Usp1Text", "Usp 1 tekst"));
            $fields->addFieldToTab("Root.Usp's", TextField::create("Usp2Icon", "Usp 2 icoon"));
            $fields->addFieldToTab("Root.Usp's", TextField::create("Usp2Text", "Usp 2 tekst"));
            $fields->addFieldToTab("Root.Usp's", TextField::create("Usp3Icon", "Usp 3 icoon"));
            $fields->addFieldToTab("Root.Usp's", TextField::create("Usp3Text", "Usp 3 tekst"));
            $fields->addFieldToTab("Root.Usp's", TextField::create("Usp4Icon", "Usp 4 icoon"));
            $fields->addFieldToTab("Root.Usp's", TextField::create("Usp4Text", "Usp 4 tekst"));
            $fields->addFieldToTab("Root.Usp's", TextField::create("Usp5Icon", "Usp 5 icoon"));
            $fields->addFieldToTab("Root.Usp's", TextField::create("Usp5Text", "Usp 5 tekst"));
            $fields->addFieldToTab("Root.Usp's", TextField::create("Usp6Icon", "Usp 6 icoon"));
            $fields->addFieldToTab("Root.Usp's", TextField::create("Usp6Text", "Usp 6 tekst"));
            
            $fields->addFieldToTab("Root.Highlight buttons", TextField::create("HighlightTitle", "Titel"));
            $fields->addFieldToTab("Root.Highlight buttons", $Afbeelding = UploadField::create('Afbeelding', 'Achtergrond'));
            $Afbeelding->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
            $Afbeelding->setAllowedMaxFileNumber(1);

            $conf2 = GridFieldConfig_RecordEditor::create();
            $conf2->addComponent(new GridFieldOrderableRows('SortHighlight'));

            $fields->addFieldToTab('Root.Highlight buttons', new GridField('Highlights', 'Highlight', $this->Highlights(), $conf2));

            $fields->removeByName("Panden");
              
            return $fields;
        }
    }
}
