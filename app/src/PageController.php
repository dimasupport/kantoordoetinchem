<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\View\Requirements;
    use SilverStripe\Core\Manifest\ModuleResourceLoader;
    use SilverStripe\Control\Director;
    use MatthiasMullie\Minify;
    
    class PageController extends ContentController
    {

        private static $allowed_actions = [];
        
        protected function init()
        {
            parent::init();
            
            $loader = SilverStripe\View\ThemeResourceLoader::inst();
            $themes = SilverStripe\View\SSViewer::get_themes();
            $themesFilePath = $loader->findThemedResource('dist/css/critical.css', $themes);
            $file = ModuleResourceLoader::singleton()->resolvePath($themesFilePath);
            $absolutePath = Director::getAbsFile($file);
            $minifier = new Minify\CSS($absolutePath);
            $mini = $minifier->minify();

Requirements::customCSS(<<<CSS
  $mini
CSS
);

        }
    }
}