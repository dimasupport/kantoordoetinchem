<?php

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;

class DataObjectExtension extends DataExtension
{
	public function updateCMSFields(FieldList $fields){
  	$fields->removeFieldFromTab('Root', 'LinkTracking');
  	$fields->removeFieldFromTab('Root', 'FileTracking');
  }
}
