<?php

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\ORM\DataExtension;
use gorriecoe\Link\Models\Link;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\GridField\GridFieldDetailForm;
use SilverStripe\Forms\GridField\GridFieldDataColumns;
use SilverStripe\Forms\GridField\GridFieldFilterHeader;
use Colymba\BulkManager\BulkAction\DeleteHandler;
use Colymba\BulkManager\BulkAction\EditHandler;
use Colymba\BulkManager\BulkAction\Handler;
use Colymba\BulkManager\BulkAction\UnlinkHandler;
use Colymba\BulkManager\BulkManager;
use Colymba\BulkUpload\BulkUploadField;
use Colymba\BulkUpload\GridFieldBulkImageUpload;
use Colymba\BulkUpload\BulkUploader;
use Colymba\BulkUpload\BulkUploadHandler;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Symbiote\GridFieldExtensions\GridFieldAddExistingSearchButton;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldDeleteAction;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\Storage\AssetStore;

class SiteConfigExtension extends DataExtension
{
    private static $db = [
        'FooterTitle' => 'Varchar(255)',
        'FooterText' => 'HTMLText',
        'Telefoonnummer' => 'Varchar(255)',        
        'GoogleMaps' => 'Text'
    ];

    private static $has_one = [
        'Afbeelding' => Image::class,
        'Privacyverklaring' => Link::class,
    ];

    private static $owns = [
        'Afbeelding'
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldToTab('Root.Footer', TextField::create('FooterTitle', 'Footer titel'));
        $fields->addFieldToTab("Root.Footer", HTMLEditorField::create("FooterText", "Footer content"));
        $fields->addFieldToTab('Root.Footer', TextField::create('GoogleMaps', 'GoogleMaps Insluit URL'));

        $fields->addFieldToTab('Root.Main', LinkField::create("Privacyverklaring", "Privacy Link", $this->owner));
        $fields->removeFieldFromTab("Root.Main", "PrivacyverklaringID");

        $fields->addFieldToTab('Root.Main', TextField::create('FormulierenEmail','Verstuur berichten naar'));        

        $fields->addFieldToTab("Root.Main", $Afbeelding = UploadField::create('Afbeelding', 'Standaard achtergrond'));
        $Afbeelding->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
        $Afbeelding->setAllowedMaxFileNumber(1);

        $fields->removeFieldFromTab('Root', 'Access');
    }


    public function cleanGoogleMaps()
    {
        $content = $this->owner->GoogleMaps;
        $newWidth = 100;
        $newHeight = 350;
        $content = preg_replace(
            array('/width="\d+"/i', '/height="\d+"/i'),
            array(sprintf('width="%d%%"', $newWidth), sprintf('height="%d"', $newHeight)),
            $content
        );
        //add class
        $content = preg_replace('/<iframe/', '<iframe class="height-rz"', $content);
        return $content;
    }
}
