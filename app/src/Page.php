<?php

namespace {

    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Assets\Image;
    use SilverStripe\Assets\Storage\AssetStore;
    use gorriecoe\Link\Models\Link;
    use gorriecoe\LinkField\LinkField;
    use SilverStripe\Forms\GridField\GridField;
    use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
    use Symbiote\GridFieldExtensions\GridFieldOrderableRows;    
    use SilverStripe\CMS\Model\SiteTree;

    class Page extends SiteTree
    {
        private static $db = [
            'AchtergrondTitle' => 'Varchar(255)'            
        ];
        private static $has_one = [
            'ContentAfbeelding' => Image::class,
            'AchtergrondAfbeelding' => Image::class,
            'ContentButton' => Link::class
        ];

        private static $many_many = [
            'Slides' => 'Slide'
        ];  

        private static $owns = [
            'Slides',
            'ContentAfbeelding',
            'AchtergrondAfbeelding'
        ];

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();

            $fields->addFieldToTab("Root.Main", $ContentAfbeelding = UploadField::create('ContentAfbeelding', 'Contentafbeelding'), 'Content');
            $ContentAfbeelding->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
            $ContentAfbeelding->setAllowedMaxFileNumber(1);

            $fields->addFieldToTab('Root.Panden', TextField::create("AchtergrondTitle", "Titel"));
            $fields->addFieldToTab("Root.Panden", $AchtergrondAfbeelding = UploadField::create('AchtergrondAfbeelding', 'Achtergrondafbeelding'), 'Content');
            $AchtergrondAfbeelding->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
            $AchtergrondAfbeelding->setAllowedMaxFileNumber(1);

            $fields->addFieldsToTab(
                'Root.Main',
                [
                  LinkField::create(
                      'ContentButton',
                      'Button',
                      $this
                  )
              ],
                'Content'
            );

            $conf = GridFieldConfig_RecordEditor::create();
            $conf->addComponent(new GridFieldOrderableRows('SortSlide'));

            $fields->addFieldToTab('Root.Slider', new GridField('Slides', 'Slides', $this->Slides(), $conf));
              
            return $fields;
        }
    }
}
