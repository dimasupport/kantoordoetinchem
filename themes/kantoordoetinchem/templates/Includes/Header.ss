<header class="header">
  <div class="container">
    <div class="row">
      <div class="col-xl-2 col-lg-3 col-md-4 col-sm-5 col-xs-5 col-6">
        <a href="$BaseHref" class="logo" rel="home">
          <figure class="image-wrap">
            <img
              alt="$SiteConfig.Title"
            	class="lazyload"
            	src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
            	data-srcset="
                $ThemeDir/dist/img/logo.png
              "
              data-aspectratio="384/99"
            	data-sizes="auto"
            />
          </figure>
        </a>
      </div>
      <div class="col-md-8 col-sm-7 col-xs-7 col-6 d-lg-none d-block my-auto">
        <button class="navigation-toggle d-print-none" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-controls="navigation"></button>
      </div>
      <div class="col-xl-10 col-lg-9 d-lg-block d-none text-right my-auto">
        <div class="right-navigation">
          <% if $SearchForm %>
          <div class="site-search-mobile-container hidden-md-up">
            <form action="/home/SearchForm" method="get" enctype="application/x-www-form-urlencoded" class="site-search-mobile">
              <input class="form-control" type="text" placeholder="Search" name="Search">
              <button class="btn btn-outline-success site-search-btn" type="submit" name="action_results" id="SearchForm_SearchForm_action_results">Search</button>
            </form>
          </div>
          <% end_if %>
          <ul class="navigation-menu">
            <% loop $Menu(1) %>
              <li class="navigation-item" >
                <a class="navigation-link" href="$Link">$MenuTitle.XML</a>
              </li>
            <% end_loop %>
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>
