<div class="highlight lazyload" data-bg="$Afbeelding.Fill(1920,630).URL">
    <div class="container">
        <div class="row d-flex">
            <div class="col-lg-12 my-auto align-items-center">
                <div class="highlight--content text-center py-5">
                    <div class="row justify-content-center">
                        <div class="col-lg-12">
                            <h4 class="highlight--titel">$HighlightTitle.RAW</h4>
                        </div>
                        <% if Highlights %>
                            <% loop Highlights %>
                                <div class="col-12 col-md-6 col-xl-3 mb-xl-5 mb-3">
                                    <a href="$Button.LinkURL" <% if $Button.OpenInNewWindow %>target="_blank"<% end_if %>>
                                        <figure class="highlight--image image-wrap mt-xl-5 mt-3">
                                            <img
                                            alt="$SiteConfig.Title"
                                                class="img-fluid lazyload"
                                                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                data-srcset="
                                                $Afbeelding.Fill(375,470).URL
                                            "
                                            data-aspectratio="384/99"
                                                data-sizes="auto"
                                            />
                                            <div class="highlight--afbeelding-titel">
                                                $Title
                                            </div>                                            
                                        </figure>
                                    </a>
                                    <a href="$Button.LinkURL" class="highlight--btn mt-3" <% if $Button.OpenInNewWindow %>target="_blank"<% end_if %>>$Button.Title</a>                                    
                                </div>
                            <% end_loop %>
                        <% end_if %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>