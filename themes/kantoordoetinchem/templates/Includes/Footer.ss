<footer class="footer pt-5 pb-3 d-print-none">
    <% if ClassName != ContactPage %>
    <div class="container py-4">
      <div class="row">
        <div class="col-xl-4">
          <h4 class="footer--title text-xl-left text-center">$SiteConfig.FooterTitle.RAW</h4>
          <div class="footer--text mt-xl-5 mt-4">
            $SiteConfig.FooterText
          </div>
        </div>

        <div class="col-xl-8">
          <div class="footer--map-holder mt-xl-0 mt-5">
            <iframe class="footer--map-holder-iframe" src="$SiteConfig.GoogleMaps" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
    <% end_if %>
    <div class="footer--copyright<% if ClassName != ContactPage %> mt-5<% end_if %>">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            &copy; $SiteConfig.Title $Now.Year | <%t Footer.AlleRechtenVoorbehouden 'Alle rechten voorbehouden' %> | <%t Footer.OntwikkelingDesign 'Ontwikkeling & Design door' %> <a href="https://www.dima.nl/" target="_blank">DIMA.</a>
          </div>
        </div>
      </div>
    </div>
</footer>