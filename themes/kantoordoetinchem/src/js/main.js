/* Imports happen in Gulp */
var stylesheet = loadCSS( ThemeStyle, document.getElementById("loadcss"));
onloadCSS( stylesheet, function() {

  WebFontConfig = {
    custom: {
      families: [
        'Font Awesome 5 Free',
        'Font Awesome 5 Brands',
        'Montserrat'
      ],

      urls: [LoadFonts]
    }
  };
  WebFont.load(WebFontConfig);

  document.addEventListener('lazybeforeunveil', function(e){
    var bg = e.target.getAttribute('data-bg');
    if(bg){
        e.target.style.backgroundImage = 'url(' + bg + ')';
    }
  });

  $(document).ready(function(){
    //Animisition page load
    $(".animsition").animsition({
        inClass: 'fade-in',
        outClass: 'fade-out',
        inDuration: 1000,
        outDuration: 300,
        linkElement: '.animsition-link',
        loading: true,
        loadingParentElement: 'body',
        loadingClass: 'animsition-loading',
        timeout: false,
        timeoutCountdown: 5000,
        onLoadEvent: true,
        browser: [ 'animation-duration', '-webkit-animation-duration'],
        overlay : false,
        overlayClass : 'animsition-overlay-slide',
        overlayParentElement : 'body',
        transition: function(url){ window.location.href = url; }
    });

    //Swiper
    var swiperBanner = new Swiper('.banner.swiper-container', {
      loop: true
    });
    var swiperSlider = new Swiper('.slider.swiper-container', {
      loop: true,
      navigation: {
        nextEl: '.slider--button-next',
        prevEl: '.slider--button-prev',
      },      
      pagination: {
        el: '.slider--pagination',
        clickable: true
      },
    });

    // Slideout menu
    var slideout = new Slideout({
      'panel': document.getElementById('panel'),
      'menu': document.getElementById('menu'),
      'padding': 350,
      'tolerance': 70,
      'side': 'right',
      'touch': false
    });
  
    document.querySelector('.navigation-toggle').addEventListener('click', function() {
      slideout.toggle();
      $(this).toggleClass('navigation-toggle--close');
    });    

    // Magnific Popup
    $('.gallerij--btn').magnificPopup({
      type: 'image',
      gallery:{
        enabled:true
      }
    });    
  });

  $('form#Form_ContactFormulier').on('submit', function (e) {
    e.preventDefault();
    grecaptcha.execute('6Lfwh6sUAAAAAHOVfhTdjY_JQkFfWpLDnKg3GlCu', { action: 'verzenden' }).then(function (token) {
        $('[name="g-recaptcha-response"]').val(token);
        $('form#Form_ContactFormulier').unbind('submit').submit();
    }, function (reason) {
        console.log(reason);
    });
  });  
});
