# SilverStripe Basis Thema
A front-end boilerplate theme for starting Bootstrap SilverStripe projects faster.

## Requirements
1. NodeJS (sudo apt-get install nodejs)
2. NPM (sudo apt-get install npm)
3. NPM legacy (sudo apt-get install nodejs-legacy)
4. Gulp (sudo npm install gulp -g)

## Installation
1. Clone or download the respository into your SilverStripe themes directory.
2. Run 'npm install' via cmd line inside the bootstarter theme folder to get all of the node dependancies, this will also install Gulp for compiling scss and js.
3. Run 'composer vendor-expose' in the project root to set up symlinks from the theme folder to the public folder.
4. Make the following changes to theme.yml under themes: rewrite_hash_links: false (rewriting hash links interferes with Bootstraps javascript).
5. Change the theme from simple to silverstripe-bootstarter in theme.yml see below for details.

```
---
Name: mytheme
---

SilverStripe\View\SSViewer:
  themes:
    - 'silverstripe-bootstarter'
    - '$default'
  rewrite_hash_links: false
```

6. Change the var PROXY_URL = 'to your local SilverStripe development url';
7. Run "gulp" via the cmd line inside bootstarter theme folder, this will compile the scss and js into css and js/dist/ and will watch for changes in the scss and js/src/ directories (building on top of the theme).
8. Run "gulp build" for production, this will minify scss and js into the dist folder.

#Post Installation
1. Add the following to the composer.json:
```
"extra": {
  "expose": [
    "dist",
    "node_modules/swiper/dist/",
    "node_modules/magnific-popup/dist/",
    "node_modules/slick-carousel/slick/",
    "node_modules/photoswipe/dist/"
  ]
}
```
2. Run 'composer vendor-expose' in the root of the project.

## Instructions
- Run "gulp watch" to watch for scss and javascript changes working with browsersync.
- Run "gulp jshint" to check for javascript errors in js/src/ from your .jshintrc file.
- Run "gulp build" to compress all files for production.

## Expose directories (Add to composer.json)
- "node_modules/magnific-popup/dist/",
- "node_modules/slick-carousel/slick/",
- "node_modules/photoswipe/dist/",
